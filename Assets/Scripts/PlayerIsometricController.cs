﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIsometricController : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] float speed = 5;


    Rigidbody2D rigidbody;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        rigidbody.velocity = new Vector2(inputX, inputY) * speed;

        if (Mathf.Abs(inputX) > 0.1f || Mathf.Abs(inputY) > 0.1f)
        {
            animator.SetFloat("SpeedX", inputX);
            animator.SetFloat("SpeedY", inputY);
        }
        //if (Mathf.Abs(inputY) > 0.1f)
        //{
        //    animator.SetFloat("SpeedY", inputY);
        //}
    }
}
