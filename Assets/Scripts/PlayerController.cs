﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] float speed;
    [SerializeField] float padding;
    [SerializeField] float fireRate = 1f;
    [SerializeField] Transform shootPosition;
    [SerializeField] int health = 100;

    [Header("Prefabs")]
    [SerializeField] LaserBullet bullet;
    [SerializeField] GameObject bomb;

    Rigidbody2D rigidbody;

    //float nextFire;
    Coroutine shootingCoroutine;    

    public int GetHealth()
    {
        return health;
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
        Move();
        Shoot();
    }

    private void Rotate()
    {
        Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Vector substraction
        Vector2 direction = mouseWorldPosition - (Vector2)transform.position;
        transform.up = direction;
    }

    private void Move()
    {
        Vector2 currentPos = transform.position;

        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        rigidbody.velocity = new Vector2(inputX, inputY) * speed;


        //float deltaX = inputX * speed * Time.deltaTime;
        //float deltaY = inputY * speed * Time.deltaTime;

        //transform.position = new Vector2(currentPos.x + deltaX, currentPos.y + deltaY);

        //float newX = Mathf.Clamp(transform.position.x, xyMin.x + padding, xyMax.x - padding);
        //float newY = Mathf.Clamp(transform.position.y, xyMin.y + padding, xyMax.y - padding);
        //transform.position = new Vector2(newX, newY);
    }

    private void Shoot()
    {
        //if (Input.GetButton("Fire1") && Time.time >= nextFire)
        //{
        //    Instantiate(bullet, shootPosition.position, transform.rotation);
        //    nextFire = Time.time + fireRate;
        //}

        //Shooting with coroutines
        if (Input.GetButtonDown("Fire1"))
        {
            shootingCoroutine = StartCoroutine(Shooting());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            //stop all coroutines
            //StopAllCoroutines();

            //stop specific coroutine
            StopCoroutine(shootingCoroutine);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Instantiate(bomb, transform.position, Quaternion.identity);
        }
    }

    IEnumerator Shooting()
    {
        while(true)
        {
            // Instantiate(bullet, shootPosition.position, transform.rotation);
            Lean.Pool.LeanPool.Spawn(bullet, shootPosition.position, transform.rotation);
            //newBullet.SendBullet(10);
            yield return new WaitForSeconds(fireRate);
        }
        //print("Start");
        //yield return new WaitForSeconds(1);
        //print("End");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if(damageDealer != null)
        {
            health -= damageDealer.GetDamage();
            Destroy(collision.gameObject);
        }
    }

}
