﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCanvas : MonoBehaviour
{
    [SerializeField] float healthUpdateTime = 1f;
    [SerializeField] Image healthBar;
    [SerializeField] EnemyController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller.OnHealthChanged += HandleHealthChange;
    }

    void HandleHealthChange(int health, int maxHealth)
    {
        float percent = (float) health / (float) maxHealth;

        StartCoroutine(UpdateHealthValue(percent));
        //healthBar.fillAmount = percent;
    }

    IEnumerator UpdateHealthValue(float percent)
    {
        float previousPercent = healthBar.fillAmount;
        float elapsedTime = 0;

        while (elapsedTime < healthUpdateTime)
        {
            elapsedTime += Time.deltaTime;

            healthBar.fillAmount = Mathf.Lerp(previousPercent, percent, elapsedTime / healthUpdateTime); 

            yield return null;
        }


        healthBar.fillAmount = percent;

    }

    private void LateUpdate()
    {
        transform.localRotation = Quaternion.Euler(0, 0, -transform.parent.eulerAngles.z);
    }
}
