﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : MonoBehaviour
{
    [SerializeField] float speed = 2;

    Rigidbody2D rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        rigidbody.velocity = transform.up * speed;
    }
    // Start is called before the first frame update
    void Start()
    {
        rigidbody.velocity = transform.up * speed;
    }

    //public void SendBullet(float speed)
    //{
    //    rigidbody.velocity = transform.up * speed;
    //}

    void OnBecameInvisible()
    {
        //Destroy(gameObject);
        Lean.Pool.LeanPool.Despawn(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Obstacle"))
        {
            //Destroy(gameObject);
            Lean.Pool.LeanPool.Despawn(gameObject);
        }
    }
}
